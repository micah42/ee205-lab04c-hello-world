###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 04c - Hello C++
#
# @file    Makefile
# @version 1.0
#
# @author Micah Chinen <micah42@hawaii.edu>
# @brief  Lab 04c - Hello C++ - EE 205 - Spr 2021
# @date   11_FEB_2021
###############################################################################

TARGETS = hello1 hello2

all: $(TARGETS)

hello1: hello1.cpp
	g++ -g -Wall -o hello1		hello1.cpp

hello2: hello2.cpp
	g++ -g -Wall -o hello2		hello2.cpp

clean:
	rm -f $(TARGETS) *.o


